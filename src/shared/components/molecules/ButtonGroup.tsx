import * as React from "react";
import {ReactNode} from "react";

interface Props {
    children?: ReactNode;
}

const ButtonGroup = (props: Props) => {
    return <div className="btn-group" role="group">
        {props.children}
    </div>
};

export default ButtonGroup;