import Endpoint from "../api";
import request from "../../http";

interface AuthParams {
    username: string,
    password: string
}

export const authenticate = async (params: AuthParams) => request(Endpoint.ObtainToken.toString(), {
    method: "POST",
    body: JSON.stringify(params)
});


interface RegistrationParams {

}