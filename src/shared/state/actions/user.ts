import {ActionType, RootAction} from "./index";
import User from "../../models/User";

export interface LoginAction extends RootAction {
    type: ActionType.Login,
    user: User
}

export function login(user: User): LoginAction {
    return {type: ActionType.Login, user}
}

export interface LogoutAction extends RootAction {
    type: ActionType.Logout
}

export function logout(): LogoutAction {
    return {type: ActionType.Logout};
}