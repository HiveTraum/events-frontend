import * as React from 'react';
import {Inputs} from "../../../components/organisms/forms/Form";
import RemotelyBuiltForm from "../../../components/organisms/forms/RemotelyBuiltForm";
import Endpoint from "../../../layers/api/api";

interface IRegistrationForm {
    username: string | null,
    password: string | null
}

type RegistrationFormType = new () => RemotelyBuiltForm<IRegistrationForm>;
const RegistrationForm = RemotelyBuiltForm as RegistrationFormType;

export default class extends React.Component {

    onChange = (inputs: Inputs<IRegistrationForm>) => {
        console.log("Changed", inputs);
    };

    onSubmit = (inputs: Inputs<IRegistrationForm>) => {
        console.log("Submitted: ", inputs);
    };

    render() {
        return <RegistrationForm endpoint={Endpoint.Users}/>
        // return <RegistrationForm onChange={this.onChange} onSubmit={this.onSubmit}>
        //     {(onChange: (field: keyof IRegistrationForm, value: IRegistrationForm[keyof IRegistrationForm]) => any) => {
        //         return [
        //             <StringInput key="username" name="username" onChange={value => {
        //                 onChange("username", value);
        //             }} label="Имя пользователя"/>,
        //
        //             <StringInput key="password" onChange={value => {
        //                 onChange("password", value)
        //             }} label="Пароль" name="password" type={InputType.Password}/>,
        //
        //             <Button label="Зарегистрироваться" key="register-button" type={Type.Submit} block={true}/>
        //         ]
        //     }}
        // </RegistrationForm>


    }
}