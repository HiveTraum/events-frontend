import * as React from 'react';
import {CSSProperties} from "react";

export enum Semantic {
    Primary = "primary",
    Secondary = "secondary",
    Success = "success",
    Danger = "danger",
    Warning = "warning",
    Info = "info",
    Light = "light",
    Dark = "dark",
    Link = "link"
}

export enum Size {
    Large = "lg",
    Small = "sm"
}

export enum Type {
    Submit = "submit",
    Button = "button"
}

interface Props {
    label: string,

    semantic?: Semantic,
    outline?: boolean,
    size?: Size,
    block?: boolean,
    disabled?: boolean,
    onClick?: () => any,
    type?: Type,
    isLoading?: boolean,
    style?: CSSProperties
}

const Button = ({
                    label, onClick, size, disabled, style,
                    semantic = Semantic.Primary,
                    outline = false,
                    block = false,
                    type = Type.Button,
                    isLoading = false
                }: Props) => {

    let className = ['btn'];
    if (outline) className.push(`btn-outline-${semantic}`);
    else className.push(`btn-${semantic}`);

    if (size) className.push(`btn-${size}`);

    if (block) className.push('btn-block');

    return <button style={style} type={type} onClick={() => {
        if (onClick) onClick();
    }} className={className.join(" ")} disabled={disabled}>{!isLoading ? label :
        <span>
            <i className="fa fa-circle-o-notch fa-spin fa-fw"/>
            <span className="sr-only">Загрузка</span>
        </span>
    }
    </button>
};

export default Button;