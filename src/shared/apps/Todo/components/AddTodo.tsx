import {connect, Dispatch} from "react-redux";
import * as React from "react";
import {addTodo} from "../../../state/actions/todo";
import Button, {Type} from "../../../components/atoms/Button";

interface Props {
    dispatch: Dispatch
}

const AddTodo = ({dispatch}: Props) => {
    let input: HTMLInputElement | null;

    return <div>
        <form onSubmit={e => {
            console.log(e);
            e.preventDefault();
            if (!input || !input.value.trim()) return;

            dispatch(addTodo(input.value));
            input.value = "";
        }}>
            <div className="input-group mb-3">
                <input ref={node => input = node} className="form-control" placeholder="Добавить задачу"/>
                <div className="input-group-append">
                    <Button label="Add Todo" type={Type.Submit}/>
                </div>
            </div>

        </form>
    </div>
};

export default connect()(AddTodo);