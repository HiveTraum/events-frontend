import TodoItem, {VisibilityFilter} from "../models/TodoItem";
import User from "../models/User";

type RootState = Readonly<{
    visibilityFilter: VisibilityFilter,
    todoItems: TodoItem[],
    user: null | User
}>;

export default RootState;

export const initialState: RootState = {
    visibilityFilter: VisibilityFilter.ShowAll,
    todoItems: [],
    user: null
};

