import {RouteProps} from "react-router";

import Home from "./apps/Home";
import About from "./apps/About";
import Entrance from './apps/Entrance/Entrance';
import Todo from "./apps/Todo/Todo";

interface ExtendedRouteProps extends RouteProps {
    title: string
}

const routes: ExtendedRouteProps[] = [
    {
        path: "/",
        exact: true,
        component: Home,
        title: 'EVENTS'
    },
    {
        path: "/hello",
        exact: true,
        component: About,
        title: "Hello"
    },
    {
        path: "/login",
        exact: true,
        component: Entrance,
        title: "Entrance"
    },
    {
        path: "/todo",
        exact: true,
        component: Todo,
        title: "Todo"
    }
];

export default routes;