import {TextInput} from "./index";
import * as React from 'react';

interface Props extends TextInput<number> {
    max?: number,
    min?: number,
}

const NumberInput = (props: Props) => {
    return <input type="text" max={props.max} min={props.min} hidden={props.hidden}
                  value={props.value === null ? "" : props.value}
                  onChange={evt => {
                      const val = parseInt(evt.target.value);
                      if (isNaN(val)) return;
                      if (props.onChange) props.onChange(val);
                  }}/>;
};

export default NumberInput;