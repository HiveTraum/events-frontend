import {AddTodoAction, SetVisibilityFilterAction, ToggleTodoAction} from "./todo";
import {Action} from "redux";
import {LoginAction, LogoutAction} from "./user";

export enum ActionType {
    AddTodo = "ADD_TODO",
    ToggleTodo = "TOGGLE_TODO",
    SetVisibilityFilter = "SET_VISIBILITY_FILTER",
    Login = "LOGIN",
    Logout = "LOGOUT"
}

export type RootAction = Action<ActionType>;

export type AppAction = AddTodoAction | ToggleTodoAction | SetVisibilityFilterAction | LoginAction | LogoutAction;

export default AppAction;