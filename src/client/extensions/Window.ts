import {PublicEnvironment} from "../../shared/layers/environment";
import RootState from "../../shared/state/RootState";

declare global {
    interface Window {
        __ENVIRONMENT__: PublicEnvironment,
        __PRELOADED_STATE__?: string | RootState,
        __REDUX_DEVTOOLS_EXTENSION__?: any,
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any
    }
}

