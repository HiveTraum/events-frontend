import * as React from "react";
import {publicEnvironment} from "../../layers/environment";
import RegistrationForm from "./components/RegistrationForm";


export default class Entrance extends React.Component {

    componentDidMount() {
        console.log(publicEnvironment);
    }

    render() {
        return <div className="container">
            <div className="row">
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div style={{marginTop: '20px'}}>
                        {/*<ul className="nav nav-pills">*/}
                            {/*<li className="nav-item">*/}
                                {/*<a className="nav-link"*/}
                            {/*</li>*/}
                        {/*</ul>*/}
                        <RegistrationForm/>
                        {/*<RegistrationForm fields={[]} onSubmit={() => {*/}
                        {/*}}/>*/}
                    </div>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6">

                </div>
            </div>
        </div>
    }
}