import {Interval} from '../date';

let firstDate = new Date(),
    secondDate = new Date();

test('Математика дат', () => {
    expect(firstDate.difference(secondDate, Interval.hour)).toBe(1);
});