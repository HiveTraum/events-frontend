import * as React from 'react';
import {TextInput} from "./index";

export enum InputType {
    Text = "text",
    Password = "password"
}

interface Props extends TextInput<string> {
    maxLength?: number,
    type?: InputType
}

const StringInput = ({type = InputType.Text, maxLength, name, label, hidden, value, onChange, helpText}: Props) => {

    const id = `input-${name}`;

    return <div className="form-group">
        <label htmlFor={id}>{label}</label>
        <input className="form-control" type={type} maxLength={maxLength} hidden={hidden}
               id={id} value={value === null ? "" : value} onChange={evt => {
            if (onChange) onChange(evt.target.value.length > 0 ? evt.target.value : null);
        }}/>
        {helpText ?
            <small className="form-text text-muted">{helpText}</small> : ""}
    </div>
};

export default StringInput;