export interface PublicEnvironment {
    API: string
}

export interface PrivateEnvironment {

}

export type Environment = PublicEnvironment & PrivateEnvironment;

export const publicEnvironment: PublicEnvironment = typeof window !== "undefined" ? (window as any).__ENVIRONMENT__ as PublicEnvironment : global.ENVIRONMENT as PublicEnvironment;


// export default Environment;
