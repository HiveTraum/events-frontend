import * as React from 'react';
import {ChangeEvent} from 'react';

interface Props {
    label: string,
    onChange?: (field: string, value: boolean) => any,
    name: string,
    value?: boolean | null
}


export default class Checkbox extends React.Component<Props> {

    onChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (this.props.onChange) this.props.onChange(this.props.name, event.target.checked);
    };

    render() {
        return <div className="form-group form-check">
            <input type="checkbox" checked={this.props.value !== null ? this.props.value : undefined} className="form-check-input" onChange={this.onChange}/>
            <label className="form-check-label">{this.props.label}</label>
        </div>
    }

}