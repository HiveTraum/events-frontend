import request from "../../http";

interface PageParams {
    page?: number,
    page_size?: number
}

interface IPaginatedList<T> {
    list: T[];
    page: number;
    next: number | null;
    prev: number | null;
    count: number;
}

class PaginatedList<T> implements IPaginatedList<T> {

    list: T[];
    next: number | null;
    page: number;
    prev: number | null;
    count: number;

    constructor({list, page, count, next = null, prev = null}: IPaginatedList<T>) {
        this.list = list;
        this.next = next;
        this.prev = prev;
        this.page = page;
        this.count = count;
    }
}

const list = (url: URL) => {
    return async <T>(params: T & PageParams): Promise<PaginatedList<T> | T[]> => {

        Object.entries(params).forEach(value => url.searchParams.append(value[0], value[1]));

        let response = await request(url.toString());

        let json = await response.json();

        if (json.results && typeof json.count === 'number') return new PaginatedList({
            list: json.results as T[],
            page: 1,
            next: json.next,
            prev: json.previous,
            count: json.count
        }); else return json as T[];
    }
};

export default list;