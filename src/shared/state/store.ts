import {compose, createStore} from "redux";
import reducer from './reducers/index';
import {initialState} from "./RootState";
import RootState from "./RootState";
import {addTodo, setVisibilityFilter, toggleTodo} from "./actions/todo";
import {VisibilityFilter} from "../models/TodoItem";

let state: RootState;

declare let window: Window;
if ((window as any).__PRELOADED_STATE__) {
    state = (window as any).__PRELOADED_STATE__ as RootState;
    delete (window as any).__PRELOADED_STATE__;
} else {
    state = initialState;
}

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, state, composeEnhancers());

export default store;