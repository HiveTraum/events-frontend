import * as React from 'react';
import {renderToString} from "react-dom/server";
import {Provider} from "react-redux";
import {StaticRouter} from "react-router";
import App from "../shared/App";
import {Request, Response} from "express-serve-static-core";
import RootState, {initialState} from "../shared/state/RootState";
import {createStore} from "redux";
import reducer from "../shared/state/reducers/index";
import {bundleTitle, staticPath} from '../../config';
// import countries from "../shared/controllers/countries";
import {PublicEnvironment} from "../shared/layers/environment";
import {publicEnvironment} from "./layers/environment";

const indexTemplate = (content: string, preloadedState: RootState, pubEnv: PublicEnvironment): string => {
    return `<html lang="ru">
	<head>
		<meta charSet="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>EVENTS</title>
		<link href="${staticPath}styles.css" rel="stylesheet"/>
	</head>
	<body>
		<div id="root">${content}</div>
		<script>
			window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\\u003c')};
			window.__ENVIRONMENT__ = ${JSON.stringify(pubEnv).replace(/</g, '\\\u003c')};
		</script>
		<script async src="${staticPath}${bundleTitle}"></script>
	</body>
	</html>`
};


export const initialHandler = async (request: Request, response: Response) => {
    let store = createStore(reducer, initialState);
    // const countriesResponse = await countries();

    const html = renderToString(
        <Provider store={store}>
            <StaticRouter location={request.url} context={{}}>
                <App/>
            </StaticRouter>
        </Provider>
    );

    response.end(indexTemplate(html, store.getState(), publicEnvironment));
};