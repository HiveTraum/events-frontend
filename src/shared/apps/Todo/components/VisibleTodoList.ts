import TodoItem, {VisibilityFilter} from "../../../models/TodoItem";
import RootState from "../../../state/RootState";
import {connect, Dispatch} from "react-redux";
import {toggleTodo} from "../../../state/actions/todo";
import TodoList from "./TodoList";

const getVisibleTodoList = (todoItems: TodoItem[], filter: VisibilityFilter): TodoItem[] => {
    switch (filter) {
        case VisibilityFilter.ShowAll:
            return todoItems;
        case VisibilityFilter.ShowCompleted:
            return todoItems.filter(t => t.completed);
        case VisibilityFilter.ShowActive:
            return todoItems.filter(t => !t.completed);
    }
};

const mapStateToProps = (state: RootState) => {
    return {
        todoItems: getVisibleTodoList(state.todoItems, state.visibilityFilter)
    }
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        onTodoClick: (id: number) => dispatch(toggleTodo(id))
    }
};

const VisibleTodoList = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList);

export default VisibleTodoList;