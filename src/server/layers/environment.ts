import {Environment, PrivateEnvironment, PublicEnvironment} from "../../shared/layers/environment";
// import '../extensions/global';

export const publicEnvironment: PublicEnvironment = {API: process.env.API} as PublicEnvironment;
const partialPrivateEnvironment: Partial<PrivateEnvironment> = {};

export const privateEnvironment: PrivateEnvironment = partialPrivateEnvironment as PrivateEnvironment;

global.ENVIRONMENT = publicEnvironment;

const environment: Environment = {...publicEnvironment, ...privateEnvironment};