import FilterLink from "./FilterLink";
import {VisibilityFilter} from "../../../models/TodoItem";
import * as React from "react";

const Footer = () => <p>
    Show:
    {' '} <FilterLink filter={VisibilityFilter.ShowAll}>All</FilterLink> {", "}
    {' '} <FilterLink filter={VisibilityFilter.ShowActive}>Active</FilterLink> {", "}
    {' '} <FilterLink filter={VisibilityFilter.ShowCompleted}>Completed</FilterLink>
</p>;

export default Footer;