import {connect, Dispatch} from "react-redux";
import Link from "../../../components/atoms/Link";
import RootState from "../../../state/RootState";
import {VisibilityFilter} from "../../../models/TodoItem";
import {setVisibilityFilter} from "../../../state/actions/todo";

interface Props {
    filter: VisibilityFilter
}

const mapStateToProps = (state: RootState, props: Props) => {
    return {
        active: props.filter === state.visibilityFilter
    }
};

const mapDispatchToProps = (dispatch: Dispatch, props: Props) => {
    return {
        onClick: () => {
            dispatch(setVisibilityFilter(props.filter))
        }
    }
};

const FilterLink = connect(
    mapStateToProps,
    mapDispatchToProps
)(Link);

export default FilterLink;