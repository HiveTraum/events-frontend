import * as React from "react";
import {ReactNode} from "react";

interface Props<T> {
    onClick?: () => void,
    active?: boolean,
    children?: ReactNode | ((obj: T) => ReactNode),
    object?: T
}

export default class ListItem<T = any> extends React.Component<Props<T>> {

    render() {

        const className = ["list-group-item"];
        if (this.props.active === true) className.push("active");

        return <li style={{cursor: this.props.onClick ? "pointer" : "auto"}} onClick={this.props.onClick}
                   className={className.join(" ")}>{this.props.children}</li>
    }
}