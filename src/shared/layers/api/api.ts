import {PublicEnvironment, publicEnvironment} from "../environment";

const URL = typeof window !== 'undefined' && window.URL ? window.URL : (global as any).URL;
//
const HOST: string = (publicEnvironment as any).API;

enum Endpoint {
    ObtainToken = new URL("token/obtain", HOST),
    Users = new URL("api/v1/users/", HOST),
    // Countries = new URL("api/v1/countries/", HOST)
}

export default Endpoint;