import * as path from 'path';

const rootPath = process.cwd();

console.log("Root path", rootPath);

export const src = path.resolve(rootPath, 'src'),
    build = path.resolve(rootPath, 'build'),
    serverSrc = path.resolve(src, 'server'),
    clientSrc = path.resolve(src, 'client'),
    clientBuild = path.resolve(build, 'client'),
    serverBuild = path.resolve(build, 'server'),
    clientEntry = path.resolve(clientSrc, 'index.tsx'),
    serverEntry = path.resolve(serverSrc, 'index.tsx'),
    staticPath = '/',
    bundleTitle = 'bundle.js';
